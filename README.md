# INFO
Files to be ignored:

- Hidden directory that is created during terraform initialization

- Terraform states files: .tfstate .tfstate.[all]

- Crash logs

- override files (if we want to override config objects)

- Terraform client profile

- Some settings ide (pycharm)
